import React from 'react';

//NumberDisplay component that renders the numbers in the game
const NumberDisplay = props => {
    return <button className="number"
                   style={{backgroundColor:colors[props.status] }}
                   onClick={() => props.onClick(props.itNum, props.status)}>
        {props.itNum}
    </button>
}

// Color Theme
const colors = {
    available: 'lightgray',
    used: 'lightgreen',
    wrong: 'lightcoral',
    candidate: 'deepskyblue',
};

export default NumberDisplay;