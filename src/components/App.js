import React from 'react';
import StarMatch from "./StarMatch";

// App redenders the StarMatch component
export default function App() {

    return (
        <div>
            <StarMatch/>
        </div>
    );
}