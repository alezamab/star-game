import React from 'react';

//PlayAgain component that controls the game and the result of the game
const PlayAgain = props => (
    <div className="game-done" style={{color: props.gameStatus === 'won'?'green':'red'}}>
        <div className="message">
            {props.gameStatus === 'won' ?'Nice Done!':'You lost, try again!'}
        </div>
        <button onClick={() => {props.onClick()}}>Play Again</button>
    </div>
);

export default PlayAgain;