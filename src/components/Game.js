import React,{useState, useEffect} from 'react';
import utils from '../utils/math-utils';
import PlayAgain from "./PlayAgain";
import NumberDisplay from "./NumberDisplay";
import DisplayStar from "./DisplayStar";

//Custom hook it keeps track of the state of the Game
const useGameState = () => {
    const [stars,setStars ] =useState(utils.random(1,9));
    const [availableNum, setAvailableNum] = useState(utils.range(1,9));
    const [candidateNum, setCandidateNum] = useState([]);
    const [secondsLeft, setSecondsLeft] = useState(10);

    //Controls the timer in the game, the game is lost if the timer reaches 0 and there are still available
    //numbers to play
    useEffect(()=>{
        if (secondsLeft > 0  && availableNum.length>0 ){
            const timerId = setTimeout(
                ()=> { setSecondsLeft(secondsLeft-1)}
                ,1000)
            return () => clearTimeout(timerId);
        }
    });

    //Controls the click of each of the numbers in the board
    const onClickNum =  (number,status) => {
        if (status == 'used' || gameStatus !== 'active'){
            return;
        }
        const newCandidateNumber = status === 'available' ?
            candidateNum.concat(number) : candidateNum.filter( cn => cn!== number);
        if (utils.sum(newCandidateNumber)!==stars) {
            setCandidateNum(newCandidateNumber)
        } else {
            const newAvailableNum = availableNum.filter( i => !newCandidateNumber.includes(i));
            setStars(utils.randomSumIn(newAvailableNum,9));
            setAvailableNum(newAvailableNum);
            setCandidateNum([]);
        }


    };
    const gameStatus = availableNum.length === 0?'won':
        secondsLeft === 0?'lost':'active';


    return {stars, availableNum, candidateNum, secondsLeft, gameStatus, onClickNum};
};

//Component that controls and renders the game
const Game = (props) => {
    const {
        stars,
        availableNum,
        candidateNum,
        secondsLeft,
        gameStatus,
        onClickNum
    } = useGameState();


    const candidatesWrong = utils.sum(candidateNum) > stars;



    const resetGame = () => {
        props.startNewGame();
    };

    const numberStatus = (number) => {
        if(!availableNum.includes(number)){
            return 'used';
        }
        if(candidateNum.includes(number)){
            return candidatesWrong ? 'wrong':'candidate';
        }
        return 'available';
    };

    return (
        <div className="game">
            <div className="help">
                Pick 1 or more numbers that sum to the number of stars
            </div>
            <div className="body">
                <div className="left">
                    {gameStatus !== 'active' ?
                        <PlayAgain onClick={resetGame} gameStatus={gameStatus}/> :
                        <DisplayStar numStars={stars} />
                    }
                </div>
                <div className="right">
                    {utils.range(1, 9).map(number => <NumberDisplay
                        key={number}
                        itNum={number}
                        status={numberStatus(number)}
                        onClick={onClickNum}
                    />)}
                </div>
            </div>
            <div className="timer">Time Remaining: {secondsLeft}</div>

        </div>
    );
};

export default Game;