import React from 'react';
import utils from '../utils/math-utils';

//Display the stars in the game
const DisplayStar = props => (
    <>
        {utils.range(1,props.numStars).map(starIt => <div key={starIt} className="star" />)}
    </>
);

export default DisplayStar;