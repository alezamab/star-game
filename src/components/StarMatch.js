// STAR MATCH - Starting Template
import React,{useState} from 'react';

import Game from './Game';

//StarMatch component that renders the game and resets the game once a game has finished
const StarMatch = () => {
    const [gameId, setGameId] = useState(1);
    const startNewGame = () => setGameId(gameId+1)
    return <Game key={gameId} startNewGame={startNewGame}/>;
};



export default StarMatch;