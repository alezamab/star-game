import React from 'react';
import ReactDOM from 'react-dom';
import './css/StarMatch.css';
import App from './components/App';

ReactDOM.hydrate(
    <App />,
    document.getElementById('mountNode'),
);